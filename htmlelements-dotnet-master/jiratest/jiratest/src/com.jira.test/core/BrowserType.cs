﻿
using System;

namespace jiratest.src.com.jira.test.core
{

    public class BrowserType
    {
        [Flags]
        public enum Browsers        {

            chrome, firefox3, ie, htmlunit
        }

        public static Browsers fromString(string value)
        {
            return (Browsers)Enum.Parse(typeof(Browsers), value);
        }
    }

}
