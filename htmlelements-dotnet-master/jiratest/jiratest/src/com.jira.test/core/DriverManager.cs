﻿using jira.test.config;
using jiratest.src.com.jira.test.config;
using log4net;
using OpenQA.Selenium;
using System;
using System.Threading;

namespace jiratest.src.com.jira.test.core
{
    public class DriverManager
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(DriverManager));
        private ExecutionContext context;

        protected const string DEFAULT_TIMEOUT = "2";
        private string waitForElementTimeout = DEFAULT_TIMEOUT;
        private long waitElementTimeout;
        //private bool seleniumIsStarted = false;
        private DriverExtension driverInstance;
        private string browserType;

        /**
       * Get Selenium object
 *
 * @return Selenium object
 */
        public DriverExtension getSelenium()
        {
            return driverInstance;
        }

        /*
         * init browser with all params, according to specified type
        */
        internal void init()
        {
            logger.Info("Init browser started");

            TestConfiguration.setSystemProperties();

            browserType = Environment.GetEnvironmentVariable(ConfigurationProperties.BROWSER_TYPE);
            //seleniumIsStarted = false;
            IWebDriver driver;
            driver = BrowserTypeFactory.createBrowser(browserType);
            driverInstance = new DriverExtension(driver);
            context = new ExecutionContext().setType(browserType).
                    setHostUrl(Environment.GetEnvironmentVariable(ConfigurationProperties.MAIN_URL));

            waitForElementTimeout = Environment.GetEnvironmentVariable(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT);
            //waitElementTimeout = long.Parse(waitForElementTimeout);

           driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(waitElementTimeout));
        }

        public void stop()
        {
            logger.Info("IN " + Thread.CurrentThread.Name + " Selenium stop");
            driverInstance.stop();
        }


        public void handleBrowserInstance()
        {
            logger.Info("IN " + Thread.CurrentThread.Name
                    + " CLEARCOOKIE!!!");
            driverInstance.clearAllCookies();
        }

        public ExecutionContext getExecutionContext()
        {
            return context;
        }

    }
}
