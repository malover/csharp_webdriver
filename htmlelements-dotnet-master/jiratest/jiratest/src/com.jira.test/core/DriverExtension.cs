﻿using System;
using OpenQA.Selenium.Internal;
using OpenQA.Selenium;
using log4net;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace jiratest.src.com.jira.test.core
{

    /**
    * Created by ALyubozhenko
    * <p/>
    * DriverExtension.class contains methods, that wrap corresponding driver methods
    */
    public class DriverExtension : IWrapsDriver
    {
        public const int WEB_DRIVER_WAIT = 90;
        private static readonly ILog logger = LogManager.GetLogger(typeof(DriverExtension));
        private const long POOLING_INTERVAL_MILLISECONDS = 500;
        protected IWebDriver driver = null;


        internal DriverExtension(IWebDriver baseDriver)
        {
            driver = baseDriver;
        }

        public IWebDriver WrappedDriver
        {
            get
            {
                return driver;
            }
        }

        internal void stop()
        {
            driver.Quit();
        }

        public void clearAllCookies()
        {
            driver.Manage().Cookies.DeleteAllCookies();
        }

        void click(IWebElement webElement)
        {
            webElement.Click();
        }

        //[Step]
        public void open(string url)
        {
            driver.Navigate().GoToUrl(url);
        }


        //[Step]
        public void safeClickElement(IWebElement element)
        {
            try
            {
                logger.Info(
                        "IN " + Thread.CurrentThread.Name + " safe click element " + element
                                .ToString() + " / " + this.ToString() + " / " + this.driver
                                .ToString());
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.ElementToBeClickable(element));
                element.Click();
            }
            catch (NoSuchElementException e)
            {
                logger.Info(" element isn't clicked  at the page ");
                logger.Error(e.Message);
                e.StackTrace.ToString();
            }
        }

        //[Step]
        public void safeClickElementAndWaitForInvisibility(IWebElement element, By locator)
        {
            try
            {
                logger.Info(
                        "IN " + Thread.CurrentThread.Name + " safe click element " + element
                                .ToString() + " / " + this.ToString() + " / " + this.driver
                                .ToString());
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));                
                element.Click();
                wait.Until(ExpectedConditions.InvisibilityOfElementLocated(locator));
            }
            catch (NoSuchElementException e)
            {
                logger.Info(" element isn't clicked  at the page ");
                logger.Error(e.Message);
                e.StackTrace.ToString();
            }
        }

        //[Step]
        public void safeEnterText(IWebElement element, string keys, By locator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                wait.Until(ExpectedConditions.ElementExists(locator));
                element.Click();
                wait.Until(ExpectedConditions.ElementToBeClickable(locator));
                element.Clear();
                element.SendKeys(keys);
            }
            catch (NoSuchElementException e)
            {
                logger.Error(e.Message);
                e.StackTrace.ToString();
            }
        }


        public IWebElement isExistsReturnWebElement(IWebElement element, By locator)
        {
            IWebElement result;
            //long timeout = long.Parse(Environment.GetEnvironmentVariable(ConfigurationProperties.WAIT_FOR_ELEMENT_TIMEOUT));
            try
            {
                driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(0));
                WebDriverWait wait =
                        new WebDriverWait(driver, TimeSpan.FromSeconds(5)); 
                wait.Until(ExpectedConditions.ElementIsVisible(locator));
                result = element;
            }
            catch (TimeoutException e)
            {
                logger.Info(" element doesn't exist");
                result = null;
                e.StackTrace.ToString();
            }
//          driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(timeout));
            return result;
        }

        /**
 * use for delay if needed
 *
 * @param waitPeriod in ms
 */
        public void waiting(long waitPeriod)
        {
                TimeSpan.FromMilliseconds(waitPeriod);
        }
    }

}




