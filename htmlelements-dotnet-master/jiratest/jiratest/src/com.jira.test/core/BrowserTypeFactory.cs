﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using log4net;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using System.Threading;

namespace jiratest.src.com.jira.test.core
{
   public class BrowserTypeFactory
    {

        private static readonly ILog logger = LogManager.GetLogger(typeof(BrowserTypeFactory));

        /**
         * Creates local web driver
         *
         * @param browserString browser name (chrome, firefox, ie, opera)
         * @return WebDriver instance
         */
        internal static IWebDriver createBrowser(String browserString)
        {
            DesiredCapabilities driverType = null;
            BrowserType.Browsers browserType = getBrowserType(browserString);
            IWebDriver driver = null;
            switch (browserType)
            {
                case BrowserType.Browsers.chrome:
                    driver = new ChromeDriver(Environment.GetEnvironmentVariable("webdriver.chrome.driver"));
                    break;
                case BrowserType.Browsers.firefox3:
                    driverType = DesiredCapabilities.Firefox();
                    driver = new FirefoxDriver(driverType);
                    break;
                case BrowserType.Browsers.ie:
                    driverType = DesiredCapabilities.InternetExplorer();
                    driver = new InternetExplorerDriver(Environment.GetEnvironmentVariable("webdriver.ie.driver"));

                    break;
                default:
                    logger.Info("Unsupported browser type " + browserType);
                    break;
            }
            if (driver != null)
            {
                logger.Info("IN " + Thread.CurrentThread.Name + "New " + browserType + "  is started: " + driver.GetType().Name);
                //driver = WebDriverFactory.getDriver(driverType);
                //if (System.getProperty(ConfigurationProperties.BROWSER_WINDOW_SIZE).equals("max"))
                driver.Manage().Window.Maximize();
            }
            return driver;
        }

        private static BrowserType.Browsers getBrowserType(String browserString)
        {
            return BrowserType.fromString(browserString);
        }

    }
}
