﻿using System;
namespace jiratest.src.com.jira.test.core
{
public class ExecutionContext
    {
        private string hostUrl;
        private string type;

        internal String getType()
        {
            return type;
        }

        internal ExecutionContext setType(String type)
        {
            this.type = type;
            return this;
        }

        internal String getHostUrl()
        {
            return hostUrl;
        }

        internal ExecutionContext setHostUrl(String hostUrl)
        {
            this.hostUrl = hostUrl;
            return this;
        }

    }
}
