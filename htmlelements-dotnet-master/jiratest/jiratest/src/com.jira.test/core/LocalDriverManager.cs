﻿
using System.Threading;

namespace jiratest.src.com.jira.test.core
{
   public sealed class LocalDriverManager
    {
        private static ThreadLocal<DriverManager> managerInstance = new ThreadLocal<DriverManager>();

        internal static DriverManager getWrapperInstance()
        {
            return managerInstance.Value;
        }

        private static void setWrapperInstance(DriverManager driver)
        {
            managerInstance.Value = driver;
        }

        public static void getDriver()
        {

            DriverManager current = new DriverManager();
            current.init();
            setWrapperInstance(current);
        }

        private LocalDriverManager()
        {
            //Do not allow to initialize this class from outside
        }
    }
}
