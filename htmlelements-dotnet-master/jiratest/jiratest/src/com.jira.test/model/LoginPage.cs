﻿
using log4net;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System.Threading;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model
{
   public class LoginPage:BaseJiraPage
    {
    private static readonly ILog logger = LogManager.GetLogger(typeof(LoginPage));
    private static string WINDOW_HEADER_CAPTION = "Jira service";

    public LoginPage()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }

    [FindsBy(How = How.CssSelector, Using = "#username")]
    private IWebElement loginField;

    [FindsBy(How = How.CssSelector, Using = "#password")]
    private IWebElement passwordField;

    [FindsBy(How = How.CssSelector, Using = "#login-submit")]
    private IWebElement submitButton;

    //[Step]
    public HomePage doLogin(string login, string password)
        {
            logger.Info("IN " + Thread.CurrentThread.Name + " Login as user " + login);
            enterLogin(login);
            localDriver.waiting(1500);
            enterPassword(password);
            localDriver.waiting(1500);
            clickSubmitButton();

            return new HomePage() ;
        }

    //[Step]
    private void enterLogin(string login)
    {
        localDriver.safeEnterText(getLoginField(), login, By.CssSelector("#page #username"));
    }

    //[Step]
    private void enterPassword(string password)
    {
            localDriver.safeEnterText(getPasswordField(), password, By.CssSelector("#page #password"));
    }

    //[Step]
    private void clickSubmitButton()
    {
            localDriver.safeClickElementAndWaitForInvisibility(getSubmitButton(), By.CssSelector("#page #login-submit"));
    }


    public IWebElement getLoginField()
    {
        return localDriver.isExistsReturnWebElement(loginField, By.CssSelector("#page #username"));
    }

    public IWebElement getPasswordField()
    {
        return localDriver.isExistsReturnWebElement(passwordField, By.CssSelector("#page #password") );
    }

    public IWebElement getSubmitButton()
    {
        return submitButton;
    }

}
}
