﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Yandex.HtmlElements.Attributes;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model
{
    [Block(How = How.CssSelector, Using = "#issue-content")]
    public class IssuePage:AbstractBase
    {

        [FindsBy(How = How.CssSelector, Using = "#summary-val")]
        private IWebElement summary;

        [FindsBy(How = How.CssSelector, Using = "#type-val")]
        private IWebElement type;

        [FindsBy(How = How.CssSelector, Using = "#environment-val")]
        private IWebElement environmentField;

        [FindsBy(How = How.CssSelector, Using = "#description-val")]
        private IWebElement description;

        public IssuePage()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }

        public string getSummary()
        {
            return summary.Text;
        }

        public string getType()
        {
            return type.Text;
        }

        public string getEnvironmentField()
        {
            return environmentField.Text;
        }

        public string getDescription()
        {
            return description.Text;
        }

    }
}
