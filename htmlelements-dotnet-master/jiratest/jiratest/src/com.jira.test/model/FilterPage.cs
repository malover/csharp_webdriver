﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model
{

    public class FilterPage : BaseJiraPage
    {

        [FindsBy(How = How.CssSelector, Using = "#searcher-query")]
        private IWebElement containsTextField;

        [FindsBy(How = How.CssSelector, Using = ".search-button")]
        private IWebElement searchButton;

        public FilterPage()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }


        public IWebElement getSearchButton()
        {
            return localDriver.isExistsReturnWebElement(searchButton, By.CssSelector("#page .search-button"));
        }

        public IWebElement getContainsTextField()
        {
            return localDriver.isExistsReturnWebElement(containsTextField, By.CssSelector("#page #searcher-query"));
        }

        //[Step]
        public void doFillInTextField(string text)
        {
            localDriver.safeEnterText(getContainsTextField(), text,By.CssSelector("#page #searcher-query"));
        }

        //[Step]
        public IssuePage doPressSearchButton()
        {
            localDriver.safeClickElement(getSearchButton());
            return new IssuePage();
        }

    }
}

