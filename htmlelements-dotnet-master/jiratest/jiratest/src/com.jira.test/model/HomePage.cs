﻿
using jiratest.src.com.jira.test.model.common.headers;
using jiratest.src.com.jira.test.model.common.menu;
using jiratest.src.com.jira.test.model.common.popup;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model
{
    public class HomePage : BaseJiraPage
    {

        public HomePage()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }

        private NavigationHeader navyHeader;

        public NavigationHeader getNavyHeader()
        {
            return navyHeader;
        }

        //[Step]
        public TicketCreationPopUp doPressCreateTicket()
        {
            getNavyHeader().doPressCreateButton();
            return new TicketCreationPopUp();
        }

        //[Step]
        public ContextMenuIssues doOpenIssues()
        {
            return getNavyHeader().doPressIssuesMenu();
        }



    }
}
