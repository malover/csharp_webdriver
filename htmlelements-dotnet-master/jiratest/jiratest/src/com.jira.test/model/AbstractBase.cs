﻿
using jiratest.src.com.jira.test.core;
using log4net;
using Yandex.HtmlElements.Elements;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model
{
   public abstract class AbstractBase:HtmlElement
    {
        static readonly ILog logger = LogManager.GetLogger(typeof(AbstractBase));
        protected DriverExtension localDriver = LocalDriverManager.getWrapperInstance().getSelenium();

        /**
 * Gets element bean and initializes this element;
 *
 * @param var1      Class of Page Object (element)
 * @param qualifier qualifier bean
 * @param <T>       page object (element) type
 * @return initialized element//where T:AbstractBase
 */

        public T getInitPageBean<T>() where T:AbstractBase  
        {
            T result =
                    HtmlElementLoader.Create<T>(localDriver.WrappedDriver);
            return result;
        }

        /**
        */
        public void buildPageObject<T>(T obj) where T : AbstractBase
        {
            HtmlElementLoader.Populate(obj, localDriver.WrappedDriver);
            
        }

    }
}
