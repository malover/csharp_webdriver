﻿/**
 * Created by ALyubozhenko
 */
public class Jissue
{

    protected const string DEFAULT_DESCRIPTION = "ALYUBOZHENKO_TEST";

    protected const string DEFAULT_ENVIRONMENT = "https://jira.atlassian.com/browse/TST";

    protected string project;

    protected string type;

    protected string summary;

    protected string environment;

    protected string description;

    private Jissue()
    {
        //private constructor
    }

    public string getProject()
    {
        return project;
    }

    public string getType()
    {
        return type;
    }

    public string getSummary()
    {
        return summary;
    }

    public string getEnvironment()
    {
        return environment;
    }

    public string getDescription()
    {
        return description;
    }

    internal static Builder newBuilder()
    {
    //    //Jissue js = new Jissue();


        return new Builder();

    }

    internal class Builder
    {
        Jissue issue;

        protected internal Builder()
        {
            // private constructor should it be!

            issue = new Jissue();
        }

        public Builder setProject(string project)
        {
            issue.project = project;

            return this;
        }

        public Builder setSummary(string summary)
        {
            issue.summary = summary;

            return this;
        }

        public Builder setEnvironment(string environment)
        {
            issue.environment = environment;

            return this;
        }

        public Builder setDescription(string description)
        {
            issue.description = description;

            return this;
        }

        public Builder setType(string type)
        {
            issue.type = type;

            return this;
        }

        public Jissue build()
        {
            return issue;
        }

    }

    public static Jissue getDefaultIssue(string type, string summary)
    {
        return newBuilder().setDescription(DEFAULT_DESCRIPTION).setEnvironment(DEFAULT_ENVIRONMENT)
                .setType(type).setSummary(summary).build();
    }

}