﻿using jira.test.config;
using System;


namespace jiratest.src.com.jira.test.model.domain
{
    public class User
    {
        private String name;
        private String password;

        private User()
        {
        }

        private User(String name, String password)
        {
            this.name = name;
            this.password = password;
        }

        public static User getTestUser()
        {
            return new User(Environment.GetEnvironmentVariable(ConfigurationProperties.TEST_USER),
                             Environment.GetEnvironmentVariable(ConfigurationProperties.TEST_PASSWORD));
        }

        public static User getAdminUser()
        {
            return new User("admin", "admin");
        }


        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getPassword()
        {
            return password;
        }

        public void setPassword(String password)
        {
            this.password = password;
        }
    }
}
