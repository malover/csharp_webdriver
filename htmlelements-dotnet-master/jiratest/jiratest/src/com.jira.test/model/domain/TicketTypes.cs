﻿using System;
using System.Collections.Generic;


namespace jiratest.src.com.jira.test.model.domain
{
 
    public class TicketTypes
    {
        private String selfType;

        TicketTypes(String type)
        {
            this.selfType = type;
        }

    public static readonly TicketTypes NewFeature = new TicketTypes("New Feature");
    public static readonly TicketTypes Bug = new TicketTypes("Bug");
    public static readonly TicketTypes Improvement = new TicketTypes("Improvement");
    public static readonly TicketTypes SupportRequest = new TicketTypes("Support Request");
    public static readonly TicketTypes Task = new TicketTypes("Task");
    public static readonly TicketTypes ThirdPartyIssue = new TicketTypes("Third-party issue");

        public static IEnumerable<TicketTypes> NextBrowserType
    {
        get
        {
            yield return NewFeature;
            yield return Bug;
            yield return Improvement;
            yield return SupportRequest;
            yield return Task;
            yield return ThirdPartyIssue;

            }
    }

        public string SelfType
        {
            get
            {
                return selfType;
            }
        }
    }







}

