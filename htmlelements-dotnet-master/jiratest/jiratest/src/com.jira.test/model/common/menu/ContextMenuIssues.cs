﻿

using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Yandex.HtmlElements.Attributes;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model.common.menu
{

    [Block(How = How.CssSelector, Using = "#find_link-content")]
    public class ContextMenuIssues : AbstractBase
    {
        [FindsBy(How = How.CssSelector, Using = "#filter_lnk_reported_lnk")]
        private IWebElement myReportedLink;

        public ContextMenuIssues()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }

        //[Step]
        public FilterPage doOpenMyReported()
        {
            localDriver.safeClickElementAndWaitForInvisibility(getMyReportedLink(), By.CssSelector("#find_link-content #filter_lnk_reported_lnk"));
            return new FilterPage();
        }

        public IWebElement getMyReportedLink()
        {
            return localDriver.isExistsReturnWebElement(myReportedLink, By.CssSelector("#find_link-content #filter_lnk_reported_lnk"));
        }


    }
}

