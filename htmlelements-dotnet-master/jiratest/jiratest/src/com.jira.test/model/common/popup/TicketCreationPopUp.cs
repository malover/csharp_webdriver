﻿
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Yandex.HtmlElements.Attributes;
using Yandex.HtmlElements.Loaders;

namespace jiratest.src.com.jira.test.model.common.popup
{

    [Block(How = How.CssSelector, Using = "#create-issue-dialog")]
    public class TicketCreationPopUp : AbstractBase
    {
        [FindsBy(How = How.CssSelector, Using = "#project-single-select")]
        private IWebElement projectCombo;

        [FindsBy(How = How.CssSelector, Using = "#issuetype-field")]
        private IWebElement issueTypeField;

        [FindsBy(How = How.XPath, Using = "//span[contains(@class, \"drop - menu\")]")]
        private IWebElement typeDropMenu;

        [FindsBy(How = How.CssSelector, Using = "#summary")]
        private IWebElement summaryTextField;

        [FindsBy(How = How.CssSelector, Using = "#environment")]
        private IWebElement envTextField;

        [FindsBy(How = How.CssSelector, Using = "#description")]
        private IWebElement descTextField;

        [FindsBy(How = How.CssSelector, Using = "#create-issue-submit")]
        private IWebElement submitButton;

        public TicketCreationPopUp()
        {
            HtmlElementLoader.PopulatePageObject(this, localDriver.WrappedDriver);
        }


        //[Step]
       private void chooseIssueType(string type)
        {
            localDriver.safeClickElement(getIssueTypeField());
            localDriver.safeEnterText(getIssueTypeField(), type, By.CssSelector("#create-issue-dialog #issuetype-field"));
//            localDriver.safeClickElement(getTypeDropMenu());
            //      getDriverExtension().safeClickElementAndWaitForInvisibility(5);
        }

        //[Step]
        private void enterSummary(string summary)
        {
            localDriver.safeEnterText(getSummaryTextField(), summary, By.CssSelector("#create-issue-dialog #summary"));
        }

        //[Step]
        private void enterEnvironment(string env)
        {
            localDriver.safeEnterText(getEnvTextField(), env, By.CssSelector("#create-issue-dialog #environment"));
        }

        //[Step]
        private void enterDescription(string desc)
        {
            localDriver.safeEnterText(getDescTextField(), desc, By.CssSelector("#create-issue-dialog #description"));
        }

        //[Step]
        private void pressCreate()
        {
            localDriver.safeClickElementAndWaitForInvisibility(getSubmitButton(), By.CssSelector("#create-issue-dialog #create-issue-submit"));
        }

        //[Step]
        public void createTicket(Jissue issue)
        {
            chooseIssueType(issue.getType());
            enterSummary(issue.getSummary());
            enterEnvironment(issue.getEnvironment());
            enterDescription(issue.getDescription());
            pressCreate();
        }


        public IWebElement getProjectCombo()
        {
            return projectCombo;
        }

        public IWebElement getIssueTypeField()
        {
            return localDriver.isExistsReturnWebElement(issueTypeField, By.CssSelector("#create-issue-dialog #issuetype-field"));
        }

        public IWebElement getSummaryTextField()
        {
            return localDriver.isExistsReturnWebElement(summaryTextField, By.CssSelector("#create-issue-dialog #summary"));
        }

        public IWebElement getEnvTextField()
        {
            return localDriver.isExistsReturnWebElement(envTextField, By.CssSelector("#create-issue-dialog #environment"));
        }

        public IWebElement getDescTextField()
        {
            return descTextField;
        }

        public IWebElement getSubmitButton()
        {
            return submitButton;
        }

        public IWebElement getTypeDropMenu()
        {
            return typeDropMenu;
        }




    }
}
