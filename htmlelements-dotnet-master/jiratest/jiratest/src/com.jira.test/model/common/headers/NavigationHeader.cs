﻿using jiratest.src.com.jira.test.model.common.menu;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Yandex.HtmlElements.Attributes;

namespace jiratest.src.com.jira.test.model.common.headers
{

    [Name("Header containing navigation buttons")]
    [Block(How = How.CssSelector, Using = ".aui-header")]
    public class NavigationHeader:AbstractBase
    {

    [FindsBy(How = How.CssSelector, Using = "#find_link")]
    private IWebElement issuesMenu;

    [FindsBy(How = How.CssSelector, Using = "#create-menu")]
    private IWebElement createButton;

        public IWebElement getCreateButton()
        {
            return createButton;
        }

        public IWebElement getIssuesMenu()
        {
            return issuesMenu;
        }


    //[Step]
    public void doPressCreateButton()
        {
            localDriver.safeClickElement(getCreateButton());
        }

    //[Step]
    public ContextMenuIssues doPressIssuesMenu()
        {
            localDriver.safeClickElement(getIssuesMenu());
            return new ContextMenuIssues();
    }


}
}
