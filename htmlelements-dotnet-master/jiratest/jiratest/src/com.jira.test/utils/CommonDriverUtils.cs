﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jiratest.src.com.jira.test.utils
{
    public static class CommonDriverUtils
    {
        public static void scrollElementByCSS(IWebDriver driver, string cssSelectorForScroll)
        {
            scrollElementByCSS(driver, cssSelectorForScroll, 0);
        }

        public static void scrollElementByCSS(IWebDriver driver, string cssSelectorForScroll,
                                              int nextStep)
        {
            IJavaScriptExecutor js = driver as IJavaScriptExecutor;
            js.ExecuteScript(
                    "var el = document.querySelector(\"" + cssSelectorForScroll + "\");el.scrollTop = "
                            + Convert.ToString(nextStep) + ";");

            System.Threading.Thread.Sleep(2);

        }

        
    }
}
