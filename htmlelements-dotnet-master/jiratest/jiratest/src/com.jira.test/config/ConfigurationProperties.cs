﻿using System;


namespace jira.test.config
{
    public static class ConfigurationProperties
    {

    public static readonly String MAIN_URL = "main_url";
    public const String WAIT_FOR_ELEMENT_TIMEOUT = "wait_timeout";
    public static String BROWSER_TYPE = "browser_type";
    public static String TEST_USER = "test_user";
    public static String TEST_PASSWORD = "test_password";
    public static String BROWSER_WINDOW_SIZE = "window_size";
    public static String DEFAULT_HUB = "default_hub";


    }
}
