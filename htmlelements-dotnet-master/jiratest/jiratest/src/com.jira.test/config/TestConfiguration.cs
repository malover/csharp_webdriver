﻿using System;
using jira.test.config;

namespace jiratest.src.com.jira.test.config
{
    class TestConfiguration
    {
        private static string url;
        private static string browser_type;
        private static string test_user;
        private static string test_password;
        private static string default_hub;
        private static string window_zise;
        private static string chromedriver_path;
        private const String ABORT_MESSAGE = " isn't specified. Test launch will be aborted";


        public static void setSystemProperties()
        {

            url = resources.global.Default.main_url;
            browser_type = resources.global.Default.browser_type;
            test_user = resources.global.Default.test_user;
            test_password = resources.global.Default.test_password;
            default_hub = resources.global.Default.default_hub;
            window_zise = resources.global.Default.window_size;
            chromedriver_path = AppDomain.CurrentDomain.BaseDirectory;
            //string ie_path = "packages\\WebDriver.IEDriver.2.44.0.1\\tools";

            //
            //ConfigurationSettings
            //
            if ((url != null) && (url.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.MAIN_URL, url);
            else throw new NotImplementedException(ConfigurationProperties.MAIN_URL + ABORT_MESSAGE);

                Environment.SetEnvironmentVariable("webdriver.chrome.driver", chromedriver_path);
                Environment.SetEnvironmentVariable("webdriver.ie.driver", chromedriver_path);

            if ((browser_type != null) && (browser_type.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.BROWSER_TYPE, browser_type);
            else throw new NotImplementedException(ConfigurationProperties.BROWSER_TYPE + ABORT_MESSAGE);

            if ((test_user != null) && (test_user.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.TEST_USER, test_user);
            else throw new NotImplementedException(ConfigurationProperties.TEST_USER + ABORT_MESSAGE);

            if ((test_password != null) && (test_password.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.TEST_PASSWORD, test_password);
            else throw new NotImplementedException(ConfigurationProperties.TEST_PASSWORD + ABORT_MESSAGE);

            if ((default_hub != null) && (default_hub.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.DEFAULT_HUB, default_hub);


            if ((window_zise != null) && (window_zise.Length > 0))
                Environment.SetEnvironmentVariable(ConfigurationProperties.BROWSER_WINDOW_SIZE, window_zise);
            else throw new NotImplementedException(ConfigurationProperties.BROWSER_WINDOW_SIZE + ABORT_MESSAGE);
        }




}
}
