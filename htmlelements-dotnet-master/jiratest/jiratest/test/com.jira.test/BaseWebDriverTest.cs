﻿using jiratest.src.com.jira.test.core;
//using MbUnit.Framework;
using NUnit.Framework;
using System;


namespace jiratest.src.com.jira.test
{
    public abstract class BaseWebDriverTest
    {
        protected internal ExecutionContext executionContext;
        protected internal String startUrl;
        protected static DriverManager selenium;

        [SetUp]
        public void setUpBeforeMethod()
        {
            if (LocalDriverManager.getWrapperInstance() == null)
            {
                LocalDriverManager.getDriver();
                selenium = LocalDriverManager.getWrapperInstance();
                executionContext = selenium.getExecutionContext();
                startUrl = executionContext.getHostUrl();
            }
        }

        [TearDown]
        public static void tearDown()
        {
            if (LocalDriverManager.getWrapperInstance() != null)
            {
                LocalDriverManager.getWrapperInstance().stop();
            }
        }

    }
}
