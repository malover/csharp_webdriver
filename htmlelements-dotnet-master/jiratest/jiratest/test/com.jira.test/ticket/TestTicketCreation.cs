﻿using System;
using jiratest.src.com.jira.test;
using System.Threading;
using jiratest.src.com.jira.test.model;
using jiratest.src.com.jira.test.model.common.menu;
using jiratest.src.com.jira.test.model.domain;
using jiratest.src.com.jira.test.model.common.popup;
using log4net;
//using MbUnit.Framework;
using NUnit.Framework;

namespace jiratest
{
//  [Parallelizable]
//  [TestFixture()]
    public class TestTicketCreation : BaseWebDriverTest
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(TestTicketCreation));
        private User testUser;
//private string[] testParams;
        private static readonly ThreadLocal<LoginPage> LOGIN_PAGE_THREAD_LOCAL = new ThreadLocal<LoginPage>();
        private static readonly ThreadLocal<HomePage> HOME_PAGE_THREAD_LOCAL = new ThreadLocal<HomePage>();
        private static readonly ThreadLocal<IssuePage> ISSUE_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL = new ThreadLocal<IssuePage>();
        private static readonly ThreadLocal<FilterPage> FILTER_RESULT_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL = new ThreadLocal<FilterPage>();
        private static readonly ThreadLocal<ContextMenuIssues> ISSUES_MENU_THREAD_LOCAL = new ThreadLocal<ContextMenuIssues>();
        private static readonly ThreadLocal<TicketCreationPopUp> TICKET_CREATION_POP_UP_THREAD_LOCAL = new ThreadLocal<TicketCreationPopUp>();

        [SetUp]
        public void localBeforeMethod()
        {
            testUser = User.getTestUser();

            logger.Info("IN " + Thread.CurrentThread.Name + " launching ");
        }

        [TestCase("New Feature")]
        [TestCase("Improvement")]
        [TestCase("Support Request")]
        [TestCase("Task")]
        public void shouldBeAbleToCreateNewDefaultJiraTicket(String ticketType)
        {
            Jissue issue = Jissue.getDefaultIssue(ticketType, "TestAL " + ticketType + (System.DateTime.Today.Millisecond.ToString()));

            openPage(startUrl);

            login(testUser);

            addNewDefaultJiraTicket(issue);

            chooseMyReportedIssues();

            filterIssuesBySummary(issue.getSummary());

            checkTicketIsCreated(issue);
        }

//#####################   Helpers part (will be moved to separate class soon) #####################//
        public void setLoginPage(LoginPage loginPage)
        {
            LOGIN_PAGE_THREAD_LOCAL.Value = loginPage;
        }

        public LoginPage getLoginPage()
        {
            return LOGIN_PAGE_THREAD_LOCAL.Value;
        }

        public void setHomePage(HomePage homePage)
        {
            HOME_PAGE_THREAD_LOCAL.Value = homePage;
        }

        public HomePage getHomePage()
        {
            return HOME_PAGE_THREAD_LOCAL.Value;
        }

        public void setIssuePage(IssuePage issuePage)
        {
            ISSUE_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL.Value = issuePage;
        }

        public IssuePage getIssuePage()
        {
            return ISSUE_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL.Value;
        }

        public void setFilterPage(FilterPage issuePage)
        {
            FILTER_RESULT_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL.Value = issuePage;
        }

        public FilterPage getFilterPage()
        {
            return FILTER_RESULT_PAGE_THREAD_LOCAL_PAGE_THREAD_LOCAL.Value;
        }

        public void setIssuesMenu(ContextMenuIssues issuePage)
        {
            ISSUES_MENU_THREAD_LOCAL.Value = issuePage;
        }

        public ContextMenuIssues getIssuesMenu()
        {
            return ISSUES_MENU_THREAD_LOCAL.Value;
        }

      //Step("Open application page")
        public void openPage(String startUrl)
        {
            selenium.getSelenium().WrappedDriver.Manage().Timeouts()
                    .SetPageLoadTimeout(TimeSpan.FromSeconds(30));
            selenium.getSelenium().WrappedDriver.Navigate().GoToUrl(startUrl);
            setLoginPage(new LoginPage());
        }

      //Step("Login")
        public void login(User user)
        {
            setHomePage(getLoginPage().doLogin(user.getName(), user.getPassword()));
            Assert.That(getHomePage()!= null, "Can't login"); // 
        }

      //Step("Choose all issues reported by current user")
        public void chooseMyReportedIssues()
        {
            setIssuesMenu(getHomePage().doOpenIssues());
            setFilterPage(getIssuesMenu().doOpenMyReported());
        }

      //Step
        public void filterIssuesBySummary(String summary)
        {
            getFilterPage().doFillInTextField(summary);
            setIssuePage(getFilterPage().doPressSearchButton());
        }

      //Step("Check that ticket created '{0}'")// commented temporarily due to the product bug
        public void checkTicketIsCreated(Jissue etalon)
        {
           //Assert.That(etalon.getDescription() == (getIssuePage().getDescription())); //"description doesn't match"
           //Assert.That(etalon.getEnvironment() == (getIssuePage().getEnvironmentField())); //"environment doesn't match"
           //Assert.That(etalon.getSummary() == (getIssuePage().getSummary())); //"summary doesn't match"
           //Assert.That(etalon.getType() == (getIssuePage().getType())); //"issue type doesn't match"
        }


        public TicketCreationPopUp getTicketCreationPopUp()
        {
            return TICKET_CREATION_POP_UP_THREAD_LOCAL.Value;
        }

        public void setTicketCreationPopUp(TicketCreationPopUp popup)
        {
            TICKET_CREATION_POP_UP_THREAD_LOCAL.Value = popup;
        }

      //Step("Add new ticket to Jira")
        public void addNewDefaultJiraTicket(Jissue issue)
        {
            setTicketCreationPopUp(getHomePage().doPressCreateTicket());
            getTicketCreationPopUp().createTicket(issue);
        }

    }
}
